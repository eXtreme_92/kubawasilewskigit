<!DOCTYPE html>
<html lang="pl-PL">
<head>
    <meta charset="UTF-8" />
    <title>Portfolio programisty i webmastera Kuby Wasilewskiego</title>
    <meta name="description" content="Portfolio programisty i webmastera Kuby Wasilewskiego" />
    <meta name="keywords" content="kuba, wasilewski, kuba wasilewski, jakub, jakub wasilewski, programista, webmaster, portfolio, css, css3, html, html5, php, js, javascript, android, c#, java" />
    <meta name="author" content="Kuba Wasilewski" />

    <meta property="og:title" content="Portfolio programisty i webmastera Kuby Wasilewskiego"/>
    <meta property="og:type" content="business.business" />
    <meta property="og:description" content="Portfolio programisty i webmastera Kuby Wasilewskiego"/>
    <meta property="og:image" content="img/orange1.png"/>
    <meta property="fb:app_id" content="" />
    <meta property="business:contact_data:street_address" content="" />
    <meta property="business:contact_data:locality" content="Koło" />
    <meta property="business:contact_data:postal_code" content="62-600" />
    <meta property="business:contact_data:country_name" content="Poland" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="robots" content="index, nofollow" />

    <link rel="icon" href="img/logo.png" />

    <link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/master/devicon.min.css" />

    <link rel="stylesheet" href="css/css.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/font-mfizz.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
    <script src="js/modernizr.js"></script> <!-- Modernizr -->

    <script src="build/react.js"></script>
    <script src="build/react-dom.js"></script>
    <script src="https://unpkg.com/babel-core@5.8.38/browser.min.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-84279201-1', 'auto');
        ga('send', 'pageview');

    </script>


</head>
<body>

<?php ob_start("ob_gzhandler"); ?>

<div id="main" class="container"></div>
<script type="text/babel" src="components/Header.js"></script>
<script type="text/babel" src="components/About.js"></script>
<script type="text/babel" src="components/Education.js"></script>
<script type="text/babel" src="components/Experience.js"></script>
<script type="text/babel" src="components/Portfolio.js"></script>
<script type="text/babel" src="components/Skills.js"></script>
<script type="text/babel" src="components/Footer.js"></script>
<script type="text/babel" src="components/Main.js"></script>
<script type="text/babel" src="components/Navbar.js"></script>
<script type="text/babel" src="components/Radial.js"></script>
<script type="text/babel" src="components/TimeLabel.js"></script>
<script type="text/babel" src="components/EducationItem.js"></script>
<script type="text/babel" src="components/PortfolioItem.js"></script>
<script type="text/babel" src="components/Video.js"></script>
<script type="text/babel" src="components/app.js"></script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>


<div class="threshold"></div>

</body>
</html>