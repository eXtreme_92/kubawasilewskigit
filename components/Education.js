window.Education = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {



            var items = [];
            var title;
            if(this.props.lang == "eng") {
                    title = "My education";
                    items = [
                            {
                                    trophies: ["Fields of education: Maths and Physics", "The Prime Minister's Scholarship"],
                                    stepsNo: "steps-one",
                                    title: "Kazimierz Wielki's High School in Koło",
                                    img: "img/kwielki.jpg",
                                    description: "2008-2011"
                            },
                            {
                                    trophies: ["BSc in Computer Sciences","The Rector's Scholarship for best students"],
                                    stepsNo: "steps-two",
                                    title: "Poznan University of Technology",
                                    img: "img/put.jpg",
                                    description: "2011-2015"
                            },
                            {
                                    trophies: ["MSc in Computer Sciences"],
                                    stepsNo: "steps-three",
                                    title: "Poznan University of Technology",
                                    img: "img/put.jpg",
                                    description: "2015-2016"
                            }

                    ];
            } else if(this.props.lang == "pol") {
                    title = "Moje wykształcenie"
                    items = [
                            {
                                    trophies: ["Profil: mat-fiz", "Stypendium Prezesa Rady Ministrów"],
                                    stepsNo: "steps-one",
                                    title: "LO im. Kazimierza Wielkiego w Kole",
                                    img: "img/kwielki.jpg",
                                    description: "2008-2011"
                            },
                            {
                                    trophies: ["Inżynier Informatyk","Stypendium Rektora dla najlepszych studentów"],
                                    stepsNo: "steps-two",
                                    title: "Politechnika Poznańska",
                                    img: "img/put.jpg",
                                    description: "2011-2015"
                            },
                            {
                                    trophies: ["Magister Informatyk"],
                                    stepsNo: "steps-three",
                                    title: "Politechnika Poznańska",
                                    img: "img/put.jpg",
                                    description: "2015-2016"
                            }
                    ];
            }



        return(


            <div id="educationPage" className="educationContainer page">

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                <div className="hiddenElem" id="he1"></div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                <div className="hiddenElem" id="he2"></div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                <div className="hiddenElem" id="he3"></div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                <div className="hiddenElem" id="he4"></div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                <div className="hiddenElem" id="he5"></div>

                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>


                <div className="educationSlider">

                    <section id="Steps" className="steps-section">

                        <h2 className="steps-header">
                                {title}
                        </h2>

                        <div className="steps-timeline">

                                {

                                        items.map(function (item, key) {
                                                return (
                                                    <EducationItem
                                                        key={key}
                                                        trophies={item.trophies}
                                                        stepsNo={item.stepsNo}
                                                        title={item.title}
                                                        img={item.img}
                                                        description={item.description}>
                                                    </EducationItem>
                                                )
                                        })

                                }

                        </div>

                    </section>

                        <br/>
                        <br/>
                        <br/>
                        <br/>
                </div>


            </div>
        );
    }
});
