
window.EducationItem = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {

        var stepsNo = this.props.stepsNo;
        var title = this.props.title;
        var img = this.props.img;
        var description = this.props.description;
        var trophies = this.props.trophies;

        return (

            <div className={stepsNo}>
                <div className="steps-content">
                    <img className="steps-img" src={img} alt={this.props.title} />
                    <div>
                        <h3 className="steps-name">
                            {title}
                        </h3>
                        <p className="steps-description">
                            {description}
                        </p>
                    </div>
                    {
                        trophies.map(function(tr, key){
                           return <p key={key} className="steps-description trophy"> <i className="fa fa-trophy"> </i> {tr} </p>
                        })
                    }
                </div>
            </div>

        );
    }
});