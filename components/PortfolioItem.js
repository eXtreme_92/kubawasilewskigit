
window.PortfolioItem = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {

        var images = this.props.images;
        var title = this.props.title;
        var button = this.props.button;
        var description = this.props.description;
        var button = this.props.button;
        if(button) {
            var address = this.props.address;
            var btn_txt = this.props.btn_txt;
        } else {
            var progress = this.props.progress;
            var progress_txt = this.props.progress_txt;
        }

        return (



                <div className="descriptionBlock">
                    <div className="lefter projImg">
                        {
                            images.map(function(el,key) {
                                return <img key={key} src={el} alt={title}></img>;
                            })
                        }
                    </div>
                    <div className="lefter projDesc">
                        <b>
                            {title}
                        </b>
                        <h6>
                            {description}
                        </h6>

                        { (button) &&
                            <a href={address} target="_blank" className="bntDownload">
                                {btn_txt}
                            </a>
                        }
                        {
                            (!button) &&
                            <h5><b>{progress_txt}</b> <progress max="100" value={progress}></progress></h5>

                        }

                    </div>
                    <div className="clearer"></div>
                </div>

        );
    }
});

