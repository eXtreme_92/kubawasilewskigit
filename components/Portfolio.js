window.Portfolio = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {


        var items = [];
        var title;
        var progress_txt;



        if(this.props.lang == "eng") {
            title = "My portfolio:";
            progress_txt = "PROGRESS:";
            items = [
                {
                    images: ["img/MSc1.png"],
                    title: "MSc Thesis: The checkers-based analysis and comparison between selected strategy-planning algorithms",
                    description: "The objective of the thesis is to point out and analyze some aspects of AI implementations that vary as different algorithms are concerned. For the purpose of analysis, there has been created an application providing GUI, which enables playing checkers. Based on theoretical knowledge as well as the research carried out with use of the application created, the thesis stands for quite a comprehensive comparison between such algorithms as: the mini-max algorithm, the mini-max algorithm with the alpha-beta pruning, the genetic algorithm. Each of them provides the ability to enter required initial parameters manually as far as the implemented application is concerned. The results of the tests done via it are – in particular – presented with use of relevant graphs, charts and in text form. The application enables the export of the neural network specification created by the genetic algorithm during the evolution process performed to a text file. This allows storing on a hard drive some kind of database that contains the specifications of neural networks which can be then examined simply by loading into the application. Some interesting feature of the app is the ability to simulate the checkers game played by two algorithms against each other, which provides the direct comparison of relevancy of using different algorithms in terms of the checkers game with respect to some initial parameters specified.",
                    button: true,
                    address: "123",
                    btn_txt: "Download project",
                    progress: "100"
                },
                {
                    images: ["img/cars1.png"],
                    title: "Car Game",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Download project",
                    progress: "65"
                },
                {
                    images: ["img/cards1.png"],
                    title: "Basic poker game multiplayer online",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Download project",
                    progress: "85"
                },
                {
                    images: ["img/orange1.png"],
                    title: "Orange BIHAPI",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Download project",
                    progress: "73"
                }

            ];
        } else if(this.props.lang == "pol") {
            title = "Moje portfolio:";
            progress_txt = "POSTĘP:";
            items = [
                {
                    images: ["img/MSc1.png"],
                    title: "Temat pracy magisterskiej: Analiza i porównanie wybranych algorytmów planowania strategicznego na przykładzie gry w warcaby",
                    description: "Celem niniejszej pracy dyplomowej jest wskazanie i analiza pewnych aspektów implementacji sztucznej inteligencji, które różnią się w zależności od zastosowanych algorytmów. na potrzeby pracy wykonano aplikację z graficznym interfejsem, pozwalającą na rozegranie partii warcabów. w oparciu o wiedzę teoretyczną dotyczącą badanych zagadnień oraz przeprowadzone z użyciem stworzonej aplikacji testy, sporządzone zostało stosunkowo obszerne porównanie pomiędzy wybranymi algorytmami, wśród których znalazły się: mini-max, mini-max z odcięciami alfa-beta oraz algorytm genetyczny. Dla każdego algorytmu zapewniono w zaimplementowanej aplikacji możliwość ręcznego podania wymaganych parametrów inicjujących. Wyniki przeprowadzanych za pośrednictwem aplikacji testów prezentowane są przez nią m. in. z użyciem stosownych grafów, wykresów, a także w formie tekstowej. Aplikacja zapewnia możliwość zapisania do pliku tekstowego specyfikacji sieci neuronowej wygenerowanej w procesie ewolucji przez algorytm genetyczny. Pozwala to utworzyć na dysku twardym komputera swoistą bazę specyfikacji sieci neuronowych, a następnie dokonywać na nich badań bezpośrednio poprzez wczytywanie w programie. Ciekawą cechą stworzonej aplikacji jest możliwość zasymulowania rozgrywki pomiędzy dwoma graczami programowymi, reprezentowanymi przez różne algorytmy – pozwala to bezpośrednio na porównanie skuteczności stosowanych algorytmów w odniesieniu do rozgrywki warcabów przy określonych parametrach inicjujących.",
                    button: false,
                    address: "projekty/mgr.zip",
                    btn_txt: "Pobierz projekt",
                    progress: "100"
                },
                {
                    images: ["img/cars1.png"],
                    title: "Car Game",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Pobierz projekt",
                    progress: "65"
                },
                {
                    images: ["img/cards1.png"],
                    title: "Basic poker game multiplayer online",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Pobierz projekt",
                    progress: "85"
                },
                {
                    images: ["img/orange1.png"],
                    title: "Orange BIHAPI",
                    description: "",
                    button: false,
                    address: "123",
                    btn_txt: "Pobierz projekt",
                    progress: "73"
                }

            ];
        }



        return (
            <div id="portfolioPage" className="portfolioContainer page">

                <b>{title}</b>

                {
                    items.map(function(item,key) {
                        return  (<PortfolioItem key={key}
                                                images={item.images}
                                                title={item.title}
                                                description={item.description}
                                                button={item.button}
                                                address={item.address}
                                                btn_txt={item.btn_txt}
                                                progress={item.progress}
                                                progress_txt={progress_txt}>
                                </PortfolioItem>)
                    })

                }

            </div>
        );
    }
});
