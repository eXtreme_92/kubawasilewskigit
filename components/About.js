window.About = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {

        var title, description, downloadBtn, cvPath;

        if(this.props.lang == "eng") {

            title = "Who am I?";
            description = "My name is Jakub. I was born on the 25th of May in 1992 (being 24 years old as of now). My adventure with web development and programming started out at the age of 12. So far, I have been given the Bachelor's (2015) and Master's (2016) degree in the field of Computer Sciences at the Poznan University of Technology. I am keen on self-development. Not only do I keep myself busy with coding nearly any time I am able to, but I have just set my heart on it. With such a passion, I always try my best doing my work. This is because I feel really strongly about miracles only meticulousness can work.";
            downloadBtn = "Download my CV in PDF";
            cvPath = "pdf/CV_Jakub_Wasilewski_eng.pdf";

        } else if(this.props.lang == "pol") {

            title = "Kim jestem?";
            description = "Nazywam się Jakub. Urodziłem się 25-ego maja 1992 r., obecnie mam 24 lata. Moja przygoda z tworzeniem witryn internetowych i programowaniem rozpoczęła się w wieku 12-stu lat. Do tej pory udało mi się zdobyć tytuły Inżyniera (2015) i Magistra (2016) Informatyka na Politechnice Poznańskiej. Jestem entuzjastą rozwoju osobistego. Nie poprzestaję na zwykłym kodowaniu kiedy tylko jest to możliwe, ale czynię to z pełnym zaangażowaniem. Tego typu zamiłowanie sprawia, że zawsze próbuję dać z siebie wszystko. Wierzę, że tylko w ten sposób można zdziałać cuda.";
            downloadBtn = "Pobierz moje CV (pdf)";
            cvPath = "pdf/CV_Jakub_Wasilewski.pdf";
        }


        return (
            <div id="aboutPage" className="aboutContainer page">

                <div id="mephoto" className="about lefter" style={{width: '35%', textAlign: 'center'}}>

                    <img className="profile_img" src="img/me.jpg" alt="Zdjęcie profilowe"/>

                </div>
                <div className="divider"></div>
                <div id="medescription" className="about lefter" style={{width: '65%'}}>

                    <div>
                    <b>{title}</b>
                    <br/>
                    <br/>
                        {description}
                    </div>

                    <br/>
                    
                    <a href={cvPath} target="_blank" className="bntDownload">
                        <img className="butIcon" src="img/Adobe-Reader.png" alt="Adobe Reader"/>
                        {downloadBtn}
                    </a>

                </div>


                <div className="clearer"></div>



            </div>


        );
    }
});
