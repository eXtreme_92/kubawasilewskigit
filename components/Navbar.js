window.Navbar = React.createClass({
    shouldComponentUpdate: function() {
        return true;
    },
    clickHandler: function(e){
        var id = e.target.id;
        this.props.clickHandler(id);
    },
    render: function() {


        var menuItems = [];
        if(this.props.lang == "pol") {
            menuItems = [
                {name: "PORTFOLIO", id: "portfolioPageOpt"},
                {name: "WYKSZTAŁCENIE", id: "he1Opt"},
                {name: "DOŚWIADCZENIE", id: "experiencePageOpt"},
                {name: "UMIEJĘTNOŚCI", id: "skillsPageOpt"},
                {name: "TROCHĘ O MNIE", id: "aboutPageOpt"}
            ];
        } else if(this.props.lang == "eng") {
            menuItems = [
                {name: "PORTFOLIO", id: "portfolioPageOpt"},
                {name: "EDUCATION", id: "he1Opt"},
                {name: "EXPERIENCE", id: "experiencePageOpt"},
                {name: "SKILLS", id: "skillsPageOpt"},
                {name: "ABOUT", id: "aboutPageOpt"}
            ];
        }


        return (

           <div className="navbar">

               <div className="righter">
                   <img id="eng" className="lang eng" src="img/england.png" onClick={this.clickHandler} alt="eng"/>
               </div>

               <div className="righter">
                   <img id="pol" className="lang pl" src="img/poland.png" onClick={this.clickHandler} alt="pol"/>
               </div>


               {

                   menuItems.map(function(item, key){

                       return (
                            <div key={key} id={item.id} className="navbarItem scroll">
                               {item.name}
                           </div>
                       )

                   })

               }



           </div>


        );
    }
});
