window.Experience = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {


        var items = [];
        var title;



        if(this.props.lang == "eng") {
            title = "My experience";
            items = [
                {
                    date: "2015",
                    icon: "img/gis.png",
                    title1: "Web developer",
                    title2: "GiS Support",
                    address: "No. 10-9a Sczanieckiej, Poznań 60-215",
                    info: "The creation of a dashboard, chart and map-based data visualization (web technologies)."
                },
                {
                    date: "September 2013",
                    icon: "img/wm.jpg",
                    title1: "Student internship",
                    title2: "Wood Mizer Industries",
                    address: "No. 114 Nagórna, Koło 62-600",
                    info: "Programming in MS SQL and C#."
                }
            ];
        } else if(this.props.lang == "pol") {
            title = "Moje doświadczenie";
            items = [
                {
                    date: "2015",
                    icon: "img/gis.png",
                    title1: "Web developer",
                    title2: "GiS Support",
                    address: "60-215 Poznań, Sczanieckiej 9a/10",
                    info: "Tworzenie dashboard'u, wizualizacja danych na mapie i wykresach (technologie webowe)."
                },
                {
                    date: "Wrzesień 2013",
                    icon: "img/wm.jpg",
                    title1: "Student-praktykant",
                    title2: "Wood Mizer Industries",
                    address: "62-600 Koło, Nagórna 114",
                    info: "Praca z MS SQL, C#."
                }
            ];
        }


        return (

<div id="experiencePage" className="experienceContainer page">
    <b>{title}</b>
    <br/>
    <br/>
            <section id="cd-timeline" className="cd-container">

                {
                    items.map(function(item, key) {
                        return (
                            <TimeLabel
                                key={key}
                                date={item.date}
                                icon={item.icon}
                                title1={item.title1}
                                title2={item.title2}
                                address={item.address}
                                info={item.info}
                            />
                        )
                    })
                }

            </section>
            
</div>
            
        );
    }
});
