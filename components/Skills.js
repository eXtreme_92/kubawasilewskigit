window.Skills = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },


    render: function () {

        var title, title1, driving, english;
        if(this.props.lang == "eng") {

            title = "Self-assessment of IT skills possessed:";
            title1 = "Other skills...";
            driving = "Driving licence: category B";
            english = "English: fluent in speaking and writing";

        } else if(this.props.lang == "pol") {

            title = "Samoocena w zakresie posiadanych umiejętności IT:";
            title1 = "Inne...";
            driving = "Prawo jazdy: kategoria B";
            english = "Angielski: biegły w mówieniu i piśmie";
        }

        return (
            <div id="skillsPage" className="skillsContainer page">


                <b>{title}</b>
                <br/>
                <br/>

                <div>
                    <Radial list={[
                        "ion-social-html5-outline",
                        "ion-social-css3-outline",
                        "icon-csharp",
                        "icon-java",
                        "devicon-photoshop-plain",
                        "devicon-gimp-plain"

                        ]} cat="a" number="95"/>



                    <Radial list={[
                        "icon-mysql",
                        "icon-apache",
                        "icon-php",
                        "ion-social-javascript-outline",
                        "devicon-bootstrap-plain",
                        "ion-social-angular-outline",
                        "devicon-react-original",
                        "devicon-inkscape-plain"



                        ]} cat="b" number="90"/>


                    <div className="clearer"></div>
                </div>

                <div className="clearer"></div>

                <div>
                    <Radial list={[
                        "ion-social-android-outline",
                        "devicon-jquery-plain",
                        "devicon-codeigniter-plain",
                        "ion-social-nodejs",
                        "ion-social-sass"

                        ]} cat="c" number="85"/>


                   <Radial list={[
                        "ion-social-tux",
                        "devicon-git-plain",
                        "devicon-github-plain",
                        "devicon-bitbucket-plain",
                        "ion-social-python"

                        ]} cat="d" number="80"/>


                    <div className="clearer"></div>
                </div>


                <div className="clearer"></div>

                <div>
                   <Radial list={[
                        "icon-ruby",
                        "devicon-cplusplus-plain",
                        "devicon-symfony-original",
                        "devicon-d3js-plain"

                        ]} cat="e" number="75"/>

                   <Radial list={[
                        "devicon-rails-plain",
                        "devicon-bower-plain",
                        "devicon-c-plain"

                        ]} cat="f" number="70"/>


                    <div className="clearer"></div>
                </div>

                <div className="clearer"></div>

                <br/>
                <br/>
                <b>{title1}</b>
                <br/>
                <br/>
                <div className="skill"><i className="fa fa-flag"> </i> {english}</div>
                <div className="skill"><i className="fa fa-car"> </i> {driving}</div>


            </div>
        );
    }
});
