window.Video = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        return (
                <video autoPlay loop className="fillWidth" id="bgvid">
                    <source src="videos/Working-Man.mp4" type="video/mp4"/>
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                    <source src="videos/Working-Man.webm" type="video/webm"/>
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
        );
    }
});

