window.Main = React.createClass({
    getInitialState: function() {
        return {lang: "pol"};
    },
    flag: true,
    setIntervalF: function() {
        this.interval = setInterval(function() {
            if(this.flag == true) {
                $("img.arrow").fadeIn();
                this.flag = false
            } else {
                $("img.arrow").fadeOut();
                this.flag = true;
            }
        }.bind(this),4000);
    },
    offset: 0.8,
    fireEduSlider: function(offset) {
        if($('#he1').offset().top <= $(window).scrollTop() + $(window).height()*offset &&
            $('#he5').offset().top > $(window).scrollTop() + $(window).height()*offset) {
            if(this.flag2 == false) {
                $('.educationSlider').fadeIn('fast');
                this.flag2 = true;
            }
        } else if($('#he5').offset().top <= $(window).scrollTop() + $(window).height()*offset) {
            $('.educationSlider').css("display","none");
            this.flag2 = false;
        } else {
            $('.educationSlider').css("display","none");
            this.flag2 = false;
        }


        if($('#he2').offset().top <= $(window).scrollTop() + $(window).height()*offset) {
            $($('.steps-content')[0]).fadeIn();
        }
        if($('#he3').offset().top <= $(window).scrollTop() + $(window).height()*offset) {
            $($('.steps-content')[1]).fadeIn();
        }
        if($('#he4').offset().top <= $(window).scrollTop() + $(window).height()*offset) {
            $($('.steps-content')[2]).fadeIn();
        }
    },
    componentDidMount: function() {
        $("img.arrow").hide();
        window.addEventListener('scroll', this.handleScroll);
        this.setIntervalF();
        $('.educationSlider').hide();
        $('.steps-content').hide();


        $(".scroll").click(function(event) {
            var id = $(this).attr('id').substring(0, $(this).attr('id').length-3);
            //alert(id);
            $('html,body').animate( { scrollTop:$('#'+id).offset().top } , 2000);
        });


        window.self = this;

        $(window).resize(function(){

            window.self.fireEduSlider.call(this,window.self.offset);

            if($(window).width()<1077){
                $('.about').removeClass('lefter');
                $('.about').css('width', '100%');

            }
            if($(window).width()>=1077){
                $('.about').addClass('lefter');
                $('#mephoto.about').css('width', '35%');
                $('#medescription.about').css('width', '65%');
            }

            if($(window).width()<863){
                $('.bgr').css('width', '100%');
            }

            if($(window).width()>=863){
                $('.bgr').css('width', '49%');
            }


            if($(window).width()<911){
                $('.projImg').removeClass('lefter').css('width', '100%');
                $('.projDesc').removeClass('lefter').css('width', '100%');
            }

            if($(window).width()>=911){
                $('.projImg').addClass('lefter').css('width', '50%');;
                $('.projDesc').addClass('lefter').css('width', '50%');;
            }
        });


    },
    componentWillUnmount: function() {
        window.removeEventListener('scroll', this.handleScroll);
        clearInterval(this.interval);
    },


    clickHandler: function(id) {

        if(id == "eng") {
            this.setState({lang: "eng"});
        } else {
            this.setState({lang: "pol"});
        }

    },


    flag1: false,
    flag2: false,


    handleScroll: function(event) {
        var scrollTop = event.target.body.scrollTop;
        var itemTranslate = scrollTop - 0.7*scrollTop;


        window.self.fireEduSlider.call(this,window.self.offset);


        var timelineBlocks = $('.cd-timeline-block'),
        offset = window.self.offset;
        var progressBars = $('.radial-progress');
        var pages = $('.page');


        if(!this.flag1) {
            //hide timeline blocks which are outside the viewport
            hideBlocks(timelineBlocks, offset);
            this.flag1=true;
        }

        function hideBlocks(blocks, offset) {
            blocks.each(function(){
                ( $(this).offset().top > $(window).scrollTop() + $(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
            });
        }

        function showBlocks(blocks, offset) {
            blocks.each(function(){

                ( $(this).offset().top <= $(window).scrollTop() + $(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
            });
        }

        (!window.requestAnimationFrame)
            ? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
            : window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });




        function fireProgress(progressBars, offset) {

            progressBars.each(function(){
                ( $(this).offset().top <= $(window).scrollTop() + $(window).height()*offset ) && $(this).attr('data-progress', $(this).attr('id'));
            });
        }

        fireProgress(progressBars, offset);



        function toggleMenu(pages, offset) {

            $('.navbarItem').css('color', 'black');

            var selected = '';
            pages.each(function(index){
                if ( $(this).offset().top-8 <= $(window).scrollTop() )  {
                    selected = index;
                }
            });



            $($('.navbarItem')[parseInt(4) - parseInt(selected)]).css('color', 'orange');

        }

        toggleMenu(pages, offset);


    },

    render: function() {

        return (
            <div className="main_container">
                <Navbar clickHandler={this.clickHandler} lang={this.state.lang}/>
                <div className="container">
                    <Header/>
                    <About lang={this.state.lang}/>
                    <Skills lang={this.state.lang}/>
                    <Experience lang={this.state.lang}/>
                    <Education lang={this.state.lang}/>
                    <Portfolio lang={this.state.lang}/>
                    <Footer lang={this.state.lang}/>
                </div>
                <Video/>
            </div>
        );
    }

});



