window.TimeLabel = React.createClass({
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {

        var icon = this.props.icon;
        var info = this.props.info;
        var title1 = this.props.title1;
        var title2 = this.props.title2;
        var address = this.props.address;
        var date = this.props.date;

        return (

            <div className="cd-timeline-block">
                <div className="cd-timeline-img cd-picture">
                    <img src={icon} alt={this.props.title2}/>
                </div>

                <div className="cd-timeline-content">
                    <h1>{title1}</h1>
                    <h4>{title2}</h4>
                    <h6><i className="fa fa-map-marker" aria-hidden="true"> </i> {address}</h6>
                    <p><i className="fa fa-info-circle" aria-hidden="true"> </i> {info}</p>
                    <span className="cd-date"><i className="fa fa-calendar" aria-hidden="true"> </i> {date}</span>
                </div>
            </div>

        );
    }
});

