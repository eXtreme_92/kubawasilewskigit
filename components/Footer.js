window.Footer = React.createClass({
    shouldComponentUpdate: function() {
        return true;
    },
    notification: function(msg) {
        if (!("Notification" in window)) {
            alert(msg);
        }

        else if (Notification.permission === "granted") {
            var notification = new Notification(msg);
        }

        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    var notification = new Notification(msg);
                } else {
                    alert(msg);
                }
            });
        } else {
            alert(msg);
        }
    },
    sendMail: function(e) {
        var address = $('#address').val();
        var msg = $('#msg').val();
        var recaptcha = grecaptcha.getResponse();

        $.post("php/mailme.php",
            {
                address: address,
                msg: msg,
                recaptcha: recaptcha
            },
            function(data, status){
                if(data == "ok") {
                    this.notification("Wiadomość została wysłana!");
                } else {
                    this.notification("Coś poszło nie tak...");
                }
            }.bind(this));


        $('#address').val("");
        $('#msg').val("");

    },
    prevdef: function(e) {
        e.preventDefault();
    },
    render: function() {

        var contactMe, message, send, loc;

        if(this.props.lang == "eng") {

            contactMe = "Contact me";
            message = "Message";
            send = "Send";
            loc = "Koło, Poland";

        } else if(this.props.lang == "pol") {

            contactMe = "Skontaktuj się ze mną";
            message = "Treść wiadomości";
            send = "Wyślij";
            loc = "Koło, Polska";
        }

        return (
            <div className="footerContainer">



                <footer className="footer-distributed">

                    <div className="footer-left">

                        <div className="footer-links">

                                <p> <i className="fa fa-map-marker fa-lg"></i> {loc}</p>
                                <p> <i className="fa fa-envelope-o "></i> <a href="mailto:kubaw_1992@op.pl?Subject=Kontakt">kubaw_1992@op.pl</a></p>
                        </div>


                        <div className="footer-icons">

                            <a href="https://pl.linkedin.com/in/jakub-wasilewski-628a21128" target="_blank"><i className="fa fa-linkedin"></i></a>

                        </div>

                    </div>



                    <div className="footer-right">

                        <p>{contactMe}</p>

                        <form method="post" action="#" onSubmit={this.prevdef}>
                            <input type="text" name="email" id="address" placeholder="Email" />
                            <textarea name="message" id="msg" placeholder={message}></textarea>
                            <div className="filler">
                                <div id="recaptcha" className="g-recaptcha" data-theme="dark" data-sitekey="6Ld6LyoTAAAAAOxP61rJbvMw9t63_olzt7AzaGNF"></div>
                            </div>
                            <button onClick={this.sendMail}>{send}</button>
                        </form>

                    </div>

                </footer>


                <div className="black">
                    <div className="foot_bl">
                        <a href="https://validator.w3.org/check?uri=www.kubawasilewski.pl&charset=%28detect+automatically%29&doctype=Inline&group=0" target="_blank"><img className="valid" src="img/valid_html5.png" alt="Valid HTML5"/></a>
                        <div className="clearer"></div>
                    </div>
                </div>

            </div>
        );
    }
});